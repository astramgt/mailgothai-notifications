# MailGoThai Notifications

------------------
This work is derived from Zimbra Notifications (MIT License)

A Chrome plugin for a quick glimpse of your MailGoThai Webmail unread messages with notifications.
Zimbra Notifications is an open source extension, you can download all of the code from [Bitbucket](https://bitbucket.org/astramgt/mailgothai-notifications/) and even install the extension from the source directory or tweak it to your own taste.
Note that the extension will not ask for your user/password and you must be already logged in to your webmail account for it to retrieve your unread messages.

### Permissions required:

 - Access your data on all websites: this is required because your Zimbra Webmail server URL is configured in the options page and cannot be know beforehand, but that is the only external location the extension tries to access.
 - Access your tabs and browsing activity: the extension will open a new tab for your inbox only if there isn't one already open.
 - Storage: for persistent settings.
 - Notifications: this one should be straightforward :)
