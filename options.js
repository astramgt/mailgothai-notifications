/**
 * Module associated with the options page for the extension.
 */
angular.module("zimbraNotificationsOptions", [])
    .controller("OptionsCtrl", function ($scope, $timeout) {

        /**
         * Saves current settings to Chrome's storage.
         */
        $scope.saveOptions = function () {
            var url = $scope.urlZimbra;
            var min = $scope.intervalMinutes;
            var dateback = $scope.dateBack;

            /**
            var mgtusername = $scope.MGTUsername;
            var mgtpassword = $scope.MGTPassword;


            if (mgtusername.length==0 || mgtpassword.length==0 ) {
              mgtusername = "user@mail.go.th";
              mgtpassword= "";
            }
            **/

            chrome.storage.sync.set({
                                        urlZimbra: url,
                                        intervalMinutes: min,
                                        dateBack : dateback,
                                        //MGTUsername: mgtusername,
                                        //MGTPassword: mgtpassword
                                    }, function() {
                                        $scope.status = "Save!"
                                        $scope.$apply();
                                        $timeout(function() {
                                            $scope.status = "";
                                            $scope.$apply();
                                        }, 1000);
                                    });
        };

        /**
         * Restores settings from Chrome's storage.
         */
        $scope.restoreOptions = function () {
            chrome.storage.sync.get({
                                        urlZimbra: "https://accounts.mail.go.th",
                                        intervalMinutes: 10,
                                        dateBack: 7
                                        //MGTUsername: "",
                                        //MGTPassword: ""
                                    }, function(items) {
                                        //$scope.urlZimbra = items.urlZimbra;
                                        $scope.urlZimbra = items.urlZimbra;
                                        $scope.intervalMinutes = items.intervalMinutes;
                                        $scope.dateBack = items.dateBack;
                                        //$scope.MGTUsername = items.MGTUsername;
                                        //$scope.MGTPassword = items.MGTPassword;
                                        $scope.$apply();
                                    });
        };

        /**
         * Initialization code for this controller.
         */
        $scope.init = function () {
            $scope.restoreOptions();
        };

        $scope.init();  // Initializes this controller.

    });
