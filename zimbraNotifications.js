/**
 * Module associated with the extension's popup.
 */
angular.module("MailGoThaiNotifications", [])
    .controller("MailCtrl", function ($scope, $timeout, $interval, $http) {

        /**
         * User unread messages.
         * @type {Array}
         */
        $scope.messages = [];

        /**
         * Stores the IDs for messages for which a notification has already been displayed.
         * @type {Array}
         */
        $scope.notifiedMessageIds = [];

        /**
         * ID used for chrome notifications.
         * @type {string}
         */
        $scope.notificationId = "newMailNotification";

        /**
         * Alerts user of an incoming message.
         * @param text the text to be used for the notification
         */
        $scope.notify = function (text) {

            $scope.clearNotification();
            var notificationOptions = {
                type: "basic",
                title: "MailGoThai Webmail",
                iconUrl: "img/mgtlogo.png",
                message: text
            };

            chrome.notifications.create($scope.notificationId, notificationOptions, function () {
            });

            $timeout(function () {
                $scope.clearNotification();
            }, 10000);
        };

        /**
         * Clears the notification sent from the extension.
         */
        $scope.clearNotification = function () {
            chrome.notifications.clear($scope.notificationId, function () {
            });
        };


        /**
         * Add/Remove Day from Current Date
         */
         $scope.addDays = function(date, days) {
             var result = new Date(date);
             result.setDate(result.getDate() + days);
             return result;
         };

        $scope.removeDays = function(date, days) {
            var result = new Date(date);
            result.setDate(result.getDate() - days);
            return result;
        };

        $scope.formatDate = function(date) {
            return (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
        }

        /**
         * Checks the user inbox for unread messages.
         * @param callbackMessages a callback function to be executed after the webmail server response
         */
        $scope.getMessages = function (callbackMessages) {
            if ($scope.promise) {
                $interval.cancel($scope.promise);
                $scope.promise = undefined;
            }

            var callback = function () {
                var afterDate = new Date();

                var data = { query: "is:unread" + " " + "after:" + $scope.formatDate($scope.removeDays(afterDate,$scope.dateBack)) ,  auth: "co" };
                var urlJson = $scope.urlParser.protocol + "//" + $scope.MGTUsername + ":" + $scope.MGTPassword + "@" +  $scope.urlParser.host + "/home/~/inbox.json";
                //var urlJson = $scope.urlParser.protocol + "//" +  $scope.urlParser.host + "/home/~/inbox.json";

                chrome.browserAction.setBadgeText({text: "0"});
                $http({ method: "GET", url: urlJson, params: data })
                    .success(function (data, status, headers, config) {
                        $scope.messages = data.m || [];

                        chrome.browserAction.setBadgeText({text: $scope.messages.length.toString()});

                        if (callbackMessages && typeof callbackMessages === "function") {
                            callbackMessages();
                        }
                    });

                $scope.scheduleRefresh();
            };

            $scope.restoreOptions(callback);
        };

        /**
         * The callback function to be used after Chrome's tabs have been retrieved.
         * @param tabs Chrome's open tabs
         */
        $scope.callbackGetTabs = function (tabs) {
            var tempLink = document.createElement("a");
            for (var j = 0; j < tabs.length; j++) {
                var tab = tabs[j];
                tempLink.href = tab.url;
                if ($scope.urlParser.host === tempLink.host) {
                    $scope.openTab = tab;
                    break;
                }
            }

            if ($scope.openTab) {
                chrome.tabs.update($scope.openTab.id, { active: true });
            } else if (!$scope.creatingTab) {
                $scope.creatingTab = true;
                chrome.tabs.create({ active: true, url: $scope.urlZimbra }, function (createdTab) {
                    $scope.openTab = createdTab;
                    $scope.creatingTab = false;
                });
            }
        };

        /**
         * Callback function for the click event both in the messages and in the notification body.
         */
        $scope.clickMessage = function () {
            $scope.openTab = undefined;
            chrome.windows.getAll(function (windows) {
                for (var i = 0; i < windows.length; i++) {
                    chrome.tabs.getAllInWindow(windows[i].id, $scope.callbackGetTabs);
                }
            });
        };

        /**
         * Schedules a refresh of the inbox unread messages.
         */
        $scope.scheduleRefresh = function () {
            if ($scope.promise) {
                $interval.cancel($scope.promise);
            }

            var interval = $scope.intervalMinutes * 60 * 1000;
            $scope.promise = $interval(function () {
                var callback = function () {
                    for (var i = 0; i < $scope.messages.length; i++) {
                        var m = $scope.messages[i];

                        // Notify user only if there hasn't already been a notification
                        // for this message's ID
                        if ($scope.notifiedMessageIds.indexOf(m.id) < 0) {
                            var text = m.e[0].p + ": " + m.su;
                            $scope.notify(text);
                            $scope.notifiedMessageIds.push(m.id);
                            break;
                        }
                    }
                };

                $scope.getMessages(callback);
            }, interval);
        };

        /**
         * Restores settings from Chrome's storage.
         * @param callback a callback function to be executed after the settings have been retrieved from storage
         */
        $scope.restoreOptions = function (callback) {
              chrome.storage.sync.get({
                                            urlZimbra: "https://accounts.mail.go.th",
                                            intervalMinutes: 10,
                                            dateBack: 7
                                            //MGTUsername: "",
                                            //MGTPassword: ""
                                    }, function(items) {
                                            $scope.urlZimbra = items.urlZimbra;
                                            $scope.urlParser.href = items.urlZimbra;
                                            $scope.intervalMinutes = items.intervalMinutes;
                                            $scope.dateBack = items.dateBack;
                                            //$scope.MGTUsername = items.MGTUsername;
                                            //$scope.MGTPassword = items.MGTPassword;
                                            callback();
                                    });
        };

        /**
         * Init function for this controller.
         */
        $scope.init = function () {
            $scope.urlParser = document.createElement("a");
            chrome.notifications.onClicked.addListener($scope.clickMessage);
            $scope.getMessages();
            chrome.runtime.onInstalled.addListener(function (details) {
                chrome.tabs.create({ url: "options.html" });
            });
        };

        $scope.init();  // Initializes this controller.

    });
